const weatherForecast = [
  {
    date: "2022-08-18T18:08:48.9070982-04:00",
    temperatureC: -16,
    temperatureF: 4,
    summary: "Sweltering",
  },
  {
    date: "2022-08-19T18:08:48.908307-04:00",
    temperatureC: 49,
    temperatureF: 120,
    summary: "Sweltering",
  },
  {
    date: "2022-08-20T18:08:48.9083124-04:00",
    temperatureC: 37,
    temperatureF: 98,
    summary: "Sweltering",
  },
  {
    date: "2022-08-21T18:08:48.9083127-04:00",
    temperatureC: 51,
    temperatureF: 123,
    summary: "Chilly",
  },
  {
    date: "2022-08-22T18:08:48.9083129-04:00",
    temperatureC: 0,
    temperatureF: 32,
    summary: "Bracing",
  },
];
